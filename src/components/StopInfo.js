import React from "react";
import { Card } from "@mui/material";

const StopInfo = ({ name, shortName }) => {
    return (
        <div>
            <h1>{name}</h1>
            <Card
                sx={{
                    display: "inline",
                    fontSize: 11,
                    border: "1px solid black",
                    paddingLeft: "3px",
                    paddingRight: "3px",
                }}
            >
                {shortName}
            </Card>
        </div>
    );
};

export default StopInfo;
