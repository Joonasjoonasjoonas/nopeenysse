import React from "react";
import { Marker, Popup } from "react-leaflet";

const DisplayClosestStops = ({
    closestStops,
    greenIcon,
    showStopDepartures,
}) => {
    const clickMarker = (stop) => {
        showStopDepartures(stop);
    };
    return (
        <div>
            {closestStops.map((stop) => {
                return (
                    <Marker
                        position={stop.coords}
                        icon={greenIcon}
                        key={stop.ID}
                        eventHandlers={{ click: () => clickMarker(stop) }}
                    >
                        <Popup key={stop.id}>
                            {stop.name}
                            <br /> {stop.shortName}
                        </Popup>
                    </Marker>
                );
            })}
        </div>
    );
};

export default DisplayClosestStops;
