import React from "react";
import Map from "./Map.js";
import { useState, useEffect } from "react";
import StopContainer from "./StopContainer.js";
import { getDistanceFromLatLonInKm } from "./Utils.js";
import ModalWindow from "./ModalWindow.js";

const AppContainer = () => {
    const [range, setRange] = useState(0.4);
    const [userLocation, setUserLocation] = useState([0, 0]);
    const [allStops, setAllStops] = useState([]);
    const [closestStops, setClosestStops] = useState([]);
    const [stopDepartures, setStopDepartures] = useState(null);
    const [selectedStop, setSelectedStop] = useState(null);
    const [allLines, setAllLines] = useState(null);
    const [errorModal, setErrorModal] = useState(false);
    const [loadingData, setLoadingData] = useState(true);

    const getUserLocation = async () => {
        const promise = await new Promise((resolve, reject) => {
            navigator.geolocation.getCurrentPosition(resolve, reject, {
                enableHighAccuracy: true,
                timeout: 10000,
                maximumAge: 0,
            });
        });

        return promise;
    };

    const getBusStops = async () => {
        const res = await fetch(
            "http://data.itsfactory.fi/journeys/api/1/stop-points"
        );
        const data = await res.json();
        return data.body;
    };

    const responseJson = async (promise) => {
        const data = await promise.json();
        return data;
    };

    const fetchStopMonitoring = async (shortName) => {
        const data = await fetch(
            `https://data.itsfactory.fi/journeys/api/1/stop-monitoring?stops=${shortName}`
        );
        return data;
    };

    const getLines = async () => {
        const res = await fetch(
            `http://data.itsfactory.fi/journeys/api/1/lines`
        );

        const data = await res.json();
        return data;
    };

    const showStopDepartures = (stop) => {
        setSelectedStop(stop);
        setLoadingData(true);
        fetchStopMonitoring(stop.shortName)
            .then((res) => responseJson(res))
            .then((res) => {
                if (Object.keys(res.body).length !== 0)
                    setStopDepartures(res.body);
                else setStopDepartures([]);
                return setLoadingData(false);
            })
            .catch(() => setErrorModal(true));
    };

    const updateClosestStops = (value) => {
        setRange(value / 1000);
    };

    useEffect(() => {
        const fetchData = async () => {
            const data = await Promise.all([
                getUserLocation(),
                getBusStops(),
                getLines(),
            ]).catch(() => setErrorModal(true));

            return data;
        };
        fetchData()
            .then((res) => {
                setUserLocation([
                    res[0].coords.latitude,
                    res[0].coords.longitude,
                ]);
                setAllStops(res[1]);
                setAllLines(res[2]);
                setLoadingData(false);
            })
            .catch(() => {
                setErrorModal(true);
            });
    }, []);

    useEffect(() => {
        const findClosestStops = () => {
            setClosestStops([]);
            const stopsToBeAdded = [];
            allStops.forEach((e, i) => {
                const busStop = {
                    ID: i,
                    shortName: e.shortName,
                    name: e.name,
                    coords: e.location.split(",").map(Number),
                };

                if (
                    getDistanceFromLatLonInKm(
                        userLocation[0],
                        userLocation[1],
                        busStop.coords[0],
                        busStop.coords[1]
                    ) <= range
                ) {
                    stopsToBeAdded.push(busStop);
                }
            });
            setClosestStops(stopsToBeAdded);
        };

        findClosestStops();
    }, [allStops, range, userLocation]);

    return (
        <div className="container">
            <div className="header-container">Nopee Nysse</div>
            <div className="stop-container">
                <StopContainer
                    selectedStop={selectedStop}
                    stopDepartures={stopDepartures}
                    allLines={allLines}
                />
            </div>
            {userLocation === { lat: null, lng: null } ? (
                <div>haetaan käyttäjän sijaintia</div>
            ) : (
                <div className="map-container">
                    <Map
                        userLocation={userLocation}
                        closestStops={closestStops}
                        updateClosestStops={updateClosestStops}
                        showStopDepartures={showStopDepartures}
                        range={range}
                    />
                </div>
            )}
            {errorModal ? (
                <ModalWindow
                    modalHeader={"Error!"}
                    modalText={"Could not fetch data..."}
                />
            ) : (
                <></>
            )}
            {loadingData ? (
                <ModalWindow
                    loadingSpinner={true}
                    modalHeader={"Loading data..."}
                />
            ) : (
                <></>
            )}

            <button
                onClick={() => {
                    setUserLocation([61.4980674, 23.7585991]);
                    updateClosestStops(400);
                }}
            >
                Keskustorille testailemaan
            </button>
        </div>
    );
};

export default AppContainer;
