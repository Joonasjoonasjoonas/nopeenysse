import React from "react";
import { Card } from "@mui/material";
import StopInfo from "./StopInfo";
import DepartureList from "./DepartureList";

const StopContainer = ({ selectedStop, stopDepartures, allLines }) => {
    return (
        <div>
            {selectedStop == null &&
            stopDepartures == null &&
            stopDepartures !== undefined ? (
                <div>Click a green marker on the map</div>
            ) : (
                <div>
                    <Card
                        style={{ paddingTop: 5, paddingLeft: 5 }}
                        variant="outlined"
                    >
                        <StopInfo
                            name={selectedStop.name}
                            shortName={selectedStop.shortName}
                        />
                        <br />
                        Next departures:
                        <br /> <br />
                        <DepartureList
                            stopDepartures={stopDepartures}
                            allLines={allLines}
                        />
                    </Card>
                </div>
            )}
        </div>
    );
};

export default StopContainer;
