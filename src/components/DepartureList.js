import React from "react";
import { Grid, Divider } from "@mui/material";

const DepartureList = ({ stopDepartures, allLines }) => {
    const getBusDescription = (lineRef) => {
        const busDescription = [...allLines.body].find(
            (object) => object.name === lineRef
        ).description;

        if (busDescription.length > 20) {
            return busDescription.slice(0, 40) + "...";
        } else return busDescription;
    };

    return (
        <>
            {stopDepartures != null ? (
                Object.values(stopDepartures)[0].map((e, i) => {
                    const depTime = new Date(e.call.expectedDepartureTime);
                    return (
                        <div
                            style={{
                                paddingTop: 10,
                                paddingBottom: 10,
                            }}
                            key={i}
                        >
                            <Grid container spacing={3} sx={{ fontSize: 13 }}>
                                <Grid item sm={1}>
                                    {e.lineRef}
                                </Grid>
                                <Grid item sm={9} sx={{ textAlign: "left" }}>
                                    {getBusDescription(e.lineRef)}
                                </Grid>
                                <Grid
                                    item
                                    sm={2}
                                    sx={{
                                        textAlign: "right",
                                        paddingRight: 1,
                                    }}
                                >
                                    {depTime.getHours()}:
                                    {depTime.getMinutes() >= 10
                                        ? depTime.getMinutes()
                                        : "0" + depTime.getMinutes()}
                                </Grid>
                            </Grid>
                            <Divider />
                        </div>
                    );
                })
            ) : (
                <></>
            )}
        </>
    );
};

export default DepartureList;
