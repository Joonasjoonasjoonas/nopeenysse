import { useMap } from "react-leaflet";

function UpdateMap({ userLocation, zoomAmt }) {
    const map = useMap();
    map.setView(userLocation, zoomAmt);

    return null;
}

export default UpdateMap;
