import React from "react";
import { MapContainer, TileLayer, Marker, Popup, Circle } from "react-leaflet";
import { Slider } from "@mui/material";
import UpdateMap from "./UpdateMap";
import * as L from "leaflet";
import DisplayClosestStops from "./DisplayClosestStops";
import { useState } from "react";

const Map = ({
    userLocation,
    closestStops,
    updateClosestStops,
    showStopDepartures,
    range,
}) => {
    const [zoomAmt, setZoomAmt] = useState(15);
    const LeafIcon = L.Icon.extend({
        options: { popupAnchor: [13, 0] },
    });

    const greenIcon = new LeafIcon({
        iconUrl:
            "https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png",
        iconSize: [21, 31], // size of the icon
        iconAnchor: [10.5, 31], // point of the icon which will correspond to marker's location
        popupAnchor: [0, -31], // point from which the popup should open relative to the iconAnchor                    ,
    });
    const blueIcon = new LeafIcon({
        iconUrl:
            "https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-blue.png",
        iconSize: [21, 31], // size of the icon
        iconAnchor: [10.5, 31], // point of the icon which will correspond to marker's location
        popupAnchor: [0, -31], // point from which the popup should open relative to the iconAnchor
    });

    const updateRange = (_, value) => {
        updateClosestStops(value);
        updateMapZoom(value);
    };

    const updateMapZoom = (value) => {
        value <= 400 && value > 250
            ? setZoomAmt(15)
            : value <= 250 && value > 100
            ? setZoomAmt(16)
            : value <= 100 && value > 50
            ? setZoomAmt(17)
            : value <= 50
            ? setZoomAmt(18)
            : setZoomAmt(zoomAmt);
    };

    return (
        <div>
            <MapContainer
                center={[0, 0]}
                scrollWheelZoom={false}
                zoomControl={false}
            >
                <UpdateMap userLocation={userLocation} zoomAmt={zoomAmt} />
                <TileLayer
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Marker position={userLocation} icon={blueIcon}>
                    <Popup>You are here</Popup>
                </Marker>

                <DisplayClosestStops
                    closestStops={closestStops}
                    greenIcon={greenIcon}
                    showStopDepartures={showStopDepartures}
                />
                <Circle center={userLocation} radius={range * 1000} />
            </MapContainer>

            <div className="slider-container">
                <Slider
                    size="medium"
                    valueLabelDisplay="auto"
                    step={50}
                    min={50}
                    max={400}
                    onChangeCommitted={updateRange}
                    defaultValue={400}
                />
                Bus Stops within range (in meters)
            </div>
        </div>
    );
};

export default Map;
