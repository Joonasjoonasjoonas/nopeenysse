# Nopee Nysse

Nopee Nysse is a React app for displaying the next several bus departures from the user's nearest bus stops inside Tampere region.
It uses Geolocation API to get the coordinates of the user, Journeys API/stop-points to get the nearest bus stops and Journeys API/stop-monitoring to get the buses and their departure times. React Leaflet is used for the map. Layout is made with CSS and MUI components.

User is able to define the display range of the bus stops with a slider.

## Install & Run

npm install

npm start
